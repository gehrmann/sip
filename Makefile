#SIP_VERSION=			4.15.5
#SIP_VERSION=			4.18
#SIP_VERSION=			4.19.4
#SIP_VERSION=			4.19.6
#SIP_VERSION=			4.19.11
SIP_VERSION=			4.19.12

TARGETS=
TARGETS+=siplib/sip.so
linux linux-i86_64: TARGETS+=sipgen/sip

# Linux
linux linux-i86_64: OS=linux
linux linux-i86_64: ARCHITECTURE=x86_64
linux linux-i86_64: build

# Android
android android-arm: OS=android
android android-arm: ARCHITECTURE=arm
android android-arm: build

build:
	OS=${OS} ARCHITECTURE=${ARCHITECTURE} SIP_VERSION=${SIP_VERSION} \
		make -f Makefile-${OS} \
			$(addprefix .build/.${OS}-${ARCHITECTURE}-${SIP_VERSION}/,${TARGETS})
